import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';
import {
    RTCView,
  } from 'react-native-webrtc';
import InCallManager from 'react-native-incall-manager';
import Draggable from 'react-native-draggable';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class videoScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            localStream: null,
            remoteStream: null,
            user: '',
            userID: this.props.user,
            client: [],
            offerfrom: '',
            call: false,
            caller: null,
            timePassed: false,
            mute: false,
            mycamera: true,
            remoteCameraStatus: false,
            remoteUser: null,
            clientID: null,
            mysocket: this.props.mysocket
          }
          this.sdp
          this.client
          this.users = this.props.user;
          this.socket = this.props.socket;
          this.candidates = [];
    }

    componentDidMount = () => {
      console.log('scoket', this.state.user);
 
     this.socket.on('cameraUser', data => {
       this.setState({
         remoteUser: data.cameraUser,
         remoteCameraStatus: data.cameraStatus
       })
       console.log('called videoUser')
     })
  }

  cameraoff = (status, cameraStat) => {
    this.socket.emit('cameraOffAlert', {
      username: this.props.user, 
      cameraOff: status, 
      clientID: this.props.client, 
      caller: this.props.from
    })
    this.setState({ mycamera: cameraStat }) 
  }

    render() {
      console.log('Props', this.props.client, this.props.from, this.socket.id)
        return(
            <View style={{ flex: 1}}>
            <View style={[styles.videosContainer]}>
            <RTCView
              key={2}
              mirror={true}
              style={{ ...styles.rtcViewRemote }}
              objectFit='cover'
              streamURL={ this.props.remoteStream && this.props.remoteStream.toURL()}
            /> 
             </View>
             {(this.state.mycamera == true) ? 
             <View style={{ flex: 1, width: windowWidth, height: windowHeight }}>
             <Draggable rendersize={60} x={windowWidth/1.45} y={windowHeight/4}>
             <View>
             <RTCView
                        key={1}
                        zOrder={1}
                        mirror={true}
                        objectFit='contain'
                        style={{ ...styles.rtcViewC }}
                        streamURL={ this.props.localStream && this.props.localStream.toURL()}
                      />
                      </View>
            </Draggable>
            </View> : 
            <View style={{ flex: 1, width: windowWidth, height: windowHeight }}>
             <Draggable rendersize={60} x={windowWidth/1.45} y={windowHeight/4}>
             <View>
             <Image source={require('../src/icons/cameraInactive.png')} style={{ width: 25, height: 25, alignSelf: 'center', top: 0}} />
             <RTCView
                        key={1}
                        zOrder={1}
                        mirror={true}
                        objectFit='contain'
                        style={{ ...styles.rtcViewC }}
                        streamURL={ this.props.localStream && this.props.localStream.toURL()}
                      />       
             </View>
            </Draggable>
            </View>
            }
            
            {(this.state.remoteCameraStatus == true) ? 
            <View>
              <Text style={{ bottom: windowHeight/2, alignSelf: 'center', color: 'white', fontWeight: '700', fontSize: 16}}>{this.state.remoteUser} turned camera off</Text>
            </View> : 
            null
            }

            <TouchableOpacity onPress={ this.props.end }>
            <Image source={require('../src/icons/redc.png')} style={{ width: 45, height: 45, marginBottom: 20, alignSelf: 'center' }} />
            </TouchableOpacity>
    
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', backgroundColor: 'black', height: 50, opacity: 0.8}}>
            {(this.state.mute == false) ? 
            <TouchableOpacity onPress={() => { InCallManager.setMicrophoneMute(true); this.setState({ mute: true}) }}>
            <Image source={require('../src/icons/mute.png')} style={{ width: 25, height: 25, marginTop: 12}}/> 
            </TouchableOpacity> :
            <TouchableOpacity onPress={() => { InCallManager.setMicrophoneMute(false); this.setState({ mute: false}) }}>
            <Image source={require('../src/icons/muteActive.png')} style={{ width: 25, height: 25, marginTop: 12}}/> 
            </TouchableOpacity>
            }
            
            {(this.state.mycamera == true) ? 
            <TouchableOpacity onPress={() => { this.props.localStream.getVideoTracks()[0].enabled = false; this.cameraoff(true, false) }}>
            <Image source={require('../src/icons/cameraActive.png')} style={{ width: 25, height: 25, marginTop: 12 }} />
            </TouchableOpacity> : 
            <TouchableOpacity onPress={() => { this.props.localStream.getVideoTracks()[0].enabled = true; this.cameraoff(false, true) }}>
            <Image source={require('../src/icons/cameraInactive.png')} style={{ width: 25, height: 25, marginTop: 12 }} />
            </TouchableOpacity>
            }
            
            <TouchableOpacity onPress={() => this.props.localStream._tracks[1]._switchCamera()}>
            <Image source={require('../src/icons/switch-cam.png')} style={{ width: 25, height: 25, marginTop: 12}} />
            </TouchableOpacity> 
    
            </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    buttonsContainer: {
      flexDirection: 'row',
    },
    absolute: {
      position: "absolute",
      top: 0,
      left: 0,
      bottom: 0,
      right: 0
    },
    button: {
      margin: 5,
      paddingVertical: 10,
      backgroundColor: 'lightgrey',
      borderRadius: 5,
    },
    textContent: {
      fontFamily: 'Avenir',
      fontSize: 20,
      textAlign: 'center',
    },
    videosContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
      backgroundColor: '#010101'
    },
    rtcView: {
      width: windowWidth, //dimensions.width,
      height: windowHeight,//dimensions.height / 2,
      backgroundColor: 'black',
    },
    rtcViewC: {
      width: windowWidth/3.2, //dimensions.width,
      height: windowHeight/3.6,//dimensions.height / 2,
      bottom: 20,
      position: 'relative',
      right: 0,
      zIndex: 2,
      overflow: 'hidden'
    },
    scrollView: {
      flex: 1,
      // flexDirection: 'row',
      backgroundColor: 'teal',
      padding: 15,
    },
    rtcViewRemote: {
      width: windowWidth,
      height: windowHeight,
      backgroundColor: 'black',
    },
    profileImg: {
      width: 50,
      height: 50,
      borderRadius: 25
    }
  });

  export default videoScreen;
