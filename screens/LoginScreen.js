import React, {useState} from 'react';
import {View, StyleSheet, StatusBar} from 'react-native';
import {Text} from 'react-native-paper';
import {TextInput} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import {Button} from 'react-native-paper';
import io from 'socket.io-client'

class LoginScreen extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      userId: '',
      loading: false,
      userID: '',
      user: [],
    }
    this.socket = null;
    this.users = [];
    this.localSocket;
  }

  componentDidMount =() =>{
    
    AsyncStorage.getItem('userId').then(id => {
      console.log(id);
      if (id) {
        this.setState({
          userID: id
        })
        
      } else {
        this.userID = null;
      }
    });

  }

  socketLogin=()=>{
    this.socket = io.connect(
      'https://turn.comvalue.com/webrtcPeer',
      {
        path: '/io/webrtc',
        query: {},
        reconnection: true,
        reconnectionDelay: 500,
        reconnectionAttempts: 10
      }
    )

    this.socket.on('connection-success', success => {
      this.localSocket= success.success;
      this.socket.emit('storeClientInfo', { username: this.state.userId });
      console.log(success);
  })

  this.socket.on('users', data => {
    data.users.map((item) => {
      this.users.push({ username: item.username, clientId: item.clientId })
      this.setState({
        user: this.users
      })
  }) 
  console.log('USERS:', this.users);
  })
  }
       
    onLogin = async() => {
      this.setState({loading: true});
      try {
        await AsyncStorage.setItem('uderId', this.state.userId);
        this.setState({loading: false});
        this.socketLogin();
        this.props.navigation.navigate('Call', {socket: this.socket, userID: this.state.userID, user: this.users });
      } catch(err) {
        console.log('Error', err);
        this.setState({ loading: false });
      }
    }

  render() {
    return(
      <View style={styles.root}>
      <StatusBar backgroundColor="#ff0000" barStyle={'dark-content'} />
        <View style={styles.content}>
          <Text style={styles.heading}>Enter your id</Text>
          <TextInput
            label="Your  ID"
            onChangeText={text => this.setState({ userId: text})}
            style={styles.input}
            borderColor={'#ff0000'}
          />
  
          <Button
            mode="contained"
            onPress={() => this.onLogin()}
            loading={this.state.loading}
            style={styles.btn}
            contentStyle={styles.btnContent}
            color={'#ff0000'}
            disabled={this.state.userId.length === 0}>
            Login
          </Button>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#010101',
    flex: 1,
    // alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    // alignSelf: 'center',
    paddingHorizontal: 20,
    justifyContent: 'center',
  },
  heading: {
    fontSize: 18,
    marginBottom: 10,
    fontWeight: '600',
    color: 'white'
  },
  input: {
    height: 60,
    marginBottom: 10,
  },
  btn: {
    height: 60,
    alignItems: 'stretch',
    justifyContent: 'center',
    fontSize: 18,
    color: '#ff0000'
  },
  btnContent: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 60,
  },
});

export default LoginScreen