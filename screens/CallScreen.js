import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Dimensions,
  FlatList,
  Image,
  TouchableWithoutFeedback,
  Alert,
  ImageBackground
} from 'react-native';
import UserAvatar from 'react-native-user-avatar';
var randomColor = require('randomcolor');
import {
  RTCPeerConnection,
  RTCIceCandidate,
  RTCSessionDescription,
  RTCView,
  MediaStream,
  MediaStreamTrack,
  mediaDevices,
  registerGlobals
} from 'react-native-webrtc';
import AsyncStorage from '@react-native-community/async-storage';
import io from 'socket.io-client'
import InCallManager from 'react-native-incall-manager';
import RNCallKeep from 'react-native-callkeep';
import { Button } from 'react-native-paper';
import Draggable from 'react-native-draggable';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
import VideoScreen from './VideoScreen';

class CallScreen extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      localStream: null,
      remoteStream: null,
      user: this.props.route.params.user,
      userID: this.props.route.params.userID,
      client: [],
      offerfrom: '',
      call: false,
      caller: null,
      timePassed: false,
      mute: false,
      mycamera: true,
      remoteCameraStatus: false,
      remoteUser: null,
      clientID: null,
    }
    this.sdp
    this.client
    this.users = [];
    this.socket = this.props.route.params.socket;
    this.candidates = [];
    this.localSocket = this.props.route.params.localSocket;
  }

  componentDidMount = () => {
     console.log('scoket', this.state.user);

    this.socket.on('offerOrAnswer', (sdp) => {
      this.sdp = JSON.stringify(sdp)
      // set sdp as remote description
      this.pc.setRemoteDescription(new RTCSessionDescription(sdp))
    })

    this.socket.on('candidate', (candidate) => {
      console.log('From Peer... ', JSON.stringify(candidate))
      // this.candidates = [...this.candidates, candidate]
      this.pc.addIceCandidate(new RTCIceCandidate(candidate))
    })

    this.socket.on('offerfrom', data => {
      this.setState({
        offerfrom: data.offerfrom,
        caller: data.caller
      })
      InCallManager.startRingtone('_BUNDLE_');
      RNCallKeep.displayIncomingCall(this.state.offerfrom, this.state.caller, "Saksham", true);
     console.log('offerfrom', data.offerfrom, this.state.caller );
    })

    // this.socket.on('cameraUser', data => {
    //   this.setState({
    //     remoteUser: data.cameraUser,
    //     remoteCameraStatus: data.cameraStatus
    //   })
    //   console.log('called videoUser')
    // })

     this.socket.on('stopCall', data => {
      this.setState({
        remoteStream: null,
        localStream: null,
        busy: false,
        caller: null,
        call: false
      })
      this.pc.removeStream(data.remoteStream);
      this.pc.removeStream(data.localStream);
      console.log('Stopping Call', this.state.remoteStream)
      })
 

    const pc_config = {
      "iceServers": [
        {
          "urls": "stun:turn.comvalue.com?transport=udp",
          "username": 'saksham',
          "credential": 'root'
        },
        {
          "urls": "turn:turn.comvalue.com?transport=tcp",
          "username": 'saksham',
          "credential": 'root'
        },
      ]
    }

    this.pc = new RTCPeerConnection(pc_config)

    this.pc.onicecandidate = (e) => {
      // send the candidates to the remote peer
      // see addCandidate below to be triggered on the remote peer
      if (e.candidate) {
        console.log(JSON.stringify(e.candidate))
        this.sendToPeer('candidate', e.candidate, this.client)
        this.sendToPeerA('candidate', e.candidate, this.state.offerfrom)
      }
    }

    // triggered when there is a change in connection state
    this.pc.oniceconnectionstatechange = (e) => {
      console.log(e)
    }

    this.pc.onaddstream = (e) => {
      console.log('Stream', e.stream)
      debugger
      // this.remoteVideoref.current.srcObject = e.streams[0]
      setTimeout(() => {
        this.setState({
          remoteStream: e.stream
        })
      }, 3000);
    }
    

    const success = (stream) => {
      console.log(stream.toURL())
      this.setState({
        localStream: stream
      })
      this.pc.addStream(stream)
    }

    const failure = (e) => {
      console.log('getUserMedia Error: ', e)
    }

    let isFront = true;
    mediaDevices.enumerateDevices().then(sourceInfos => {
      console.log(sourceInfos);
      let videoSourceId;
      for (let i = 0; i < sourceInfos.length; i++) {
        const sourceInfo = sourceInfos[i];
        if (sourceInfo.kind == "videoinput" && sourceInfo.facing == (isFront ? "front" : "environment")) {
          videoSourceId = sourceInfo.deviceId;
        }
      }

      const constraints = {
        audio: true,
        video: {
          mandatory: {
            minWidth: 1024, // Provide your own width, height and frame rate here
            minHeight: 800,
            minFrameRate: 30
          },
          facingMode: (isFront ? "user" : "environment"),
          optional: (videoSourceId ? [{ sourceId: videoSourceId }] : [])
        }
      }

      mediaDevices.getUserMedia(constraints)
        .then(success)
        .catch(failure);
    });
  }

  sendToPeer = (messageType, payload, clientID) => {
    this.socket.emit(messageType, {
      socketID: this.socket.id,
      payload,
      clientID
    })
    console.log('sendtopeer',messageType, this.socket.id, clientID);
  }

  sendToPeerA = (messageType, payload, clientID) => {
    this.socket.emit(messageType, {
      socketID: this.socket.id,
      payload,
      clientID
    })
    console.log('sendtopeer',messageType, this.socket.id, clientID);
  }

  onButton = (clientID) => {
    this.client= clientID;
    this.setState({
      call: true
    })
  }

  // cameraoff = (status, cameraStat) => {
  //   this.socket.emit('cameraOffAlert', {
  //     username: this.state.userID, 
  //     cameraOff: status, 
  //     clientID: this.state.clientID, 
  //     caller: this.state.offerfrom
  //   })
  //   this.setState({ mycamera: cameraStat }) 
  // }

  createOffer = (clientID) => {
    InCallManager.start({ media: "audio" });

    console.log('Offer')
    // initiates the creation of SDP
    this.pc.createOffer({ offerToReceiveVideo: 1 })
      .then(sdp => {
        // set offer sdp as local description
        this.pc.setLocalDescription(sdp)
        this.sendToPeer('offer', sdp, clientID)
      })
  }

  createAnswer = () => {
    console.log('Answer')
    this.setState({
      busy: true,
      caller: null
    })

    this.pc.createAnswer({ offerToReceiveVideo: 1 })
      .then(sdp => {
        // set answer sdp as local description
        this.pc.setLocalDescription(sdp)
        this.sendToPeer('Answer', sdp, this.state.offerfrom)
        console.log('OfferFrom', this.state.offerfrom)
      })
      InCallManager.start({ media: "audio" });
      InCallManager.stopRingtone(); 
  }

  setRemoteDescription = () => {
    // retrieve and parse the SDP copied from the remote peer
    const desc = JSON.parse(this.sdp)
    // set sdp as remote description
    this.pc.setRemoteDescription(new RTCSessionDescription(desc))
  }

  addCandidate = () => {

    this.candidates.forEach(candidate => {
      console.log(JSON.stringify(candidate))
      this.pc.addIceCandidate(new RTCIceCandidate(candidate))
    });
  }

  endcall = (remoteStream, localStream) => {
    this.socket.emit('endCall', {
      caller: this.socket.id, 
      callee: this.state.offerfrom, 
      local: localStream, 
      remote: remoteStream})
  }

  render() {
    const { navigation } = this.props;
    const {
      localStream,
      remoteStream,
    } = this.state
    
    const remoteVideo = remoteStream ?
      (
        <VideoScreen navigation={navigation} 
        localStream={localStream} 
        remoteStream={remoteStream} 
        socket={this.props.route.params.socket} 
        user={this.state.userID} 
        client={this.state.clientID} 
        from={this.state.offerfrom}
        users={this.users}
        end={() => this.endCall(localStream, remoteStream)}
        />
      ) :
      (
        <View style={{ flex: 1 }}>
        <View style={styles.videosContainer}>
          <RTCView
                    key={1}
                    zOrder={0}
                    mirror={true}
                    objectFit='cover'
                    style={{ ...styles.rtcView }}
                    streamURL={localStream && localStream.toURL()}
                  />
        </View>
        <TouchableOpacity onPress={() => { this.endcall(remoteStream, localStream)  }}>
        <Image source={require('../src/icons/redc.png')} style={{ width: 45, height: 45, marginBottom: 40, alignSelf: 'center' }} />
        </TouchableOpacity>
        </View>
        
      )

    return (

      <SafeAreaView style={{ flex: 1, }}>
      <StatusBar backgroundColor="#ff0000" barStyle={'dark-content'} />
      <View style={styles.videosContainer}> 

      <View>
      
      </View>
      <FlatList 
        data={this.state.user}
        extraData={this.state.user}
        keyExtractor={(item, index) => index.toString()} 
                 renderItem= { ({item}) => (
                   <View style={{ backgroundColor: '#69779b', marginBottom: 10}}>
                  <TouchableOpacity>
                   
                   {(item.username == this.state.userID) ? 
                      null : 
                      <View style={{ backgroundColor: '#33313b', flex: 1, flexDirection: 'row', height: 80 }}>
                      <UserAvatar style={{height: 60, width: 60, borderRadius: 30, marginTop: 10, marginLeft: 10 }} name={item.username} bgColors={[randomColor({
                      luminosity: 'light',
                      hue: 'blue'})]} />
                      {/* <Image source={require('../avatar.png')} style={{height: 60, width:60, borderRadius: 30, marginTop: 10, marginLeft: 10 }} /> */}
                      <Text style={{color: 'white', fontSize: 15, marginTop: 32.5, margin: 10, marginLeft: windowWidth/5 ,position: 'absolute'}}>{item.username}</Text>
                      <TouchableOpacity onPress={() => { this.onButton(item.clientId), this.createOffer(item.clientId); this.setState({ clientID: item.clientId}) } } >
                      <Image source={require('../src/icons/vs.png')} style={{height: 25, width: 25, marginTop: 25, marginLeft: windowWidth/1.75 }}/>
                      </TouchableOpacity>
                      
                      <TouchableOpacity>
                      <Image source={require('../src/icons/vc.png')} style={{height: 25, width: 25, marginTop: 25, marginLeft: windowWidth/11 }}/>
                      </TouchableOpacity>
                      
                      </View> }  
                  </TouchableOpacity>

                  </View>
                  )}
             />
            <View>
            {(this.state.call == true) ?
            <View style={{ flex: 1}}>
            <StatusBar hidden />  
            {remoteVideo}         
            </View> 
             : 
             (this.state.caller != null) ? 
             <View style={{ flex: 1}}>
               <View>
                  <Text style={{ color: 'white', alignSelf: 'center', fontSize: 30 }}>{this.state.caller} Calling ....... </Text>
               </View>
               <Button mode="text" onPress={() => this.createAnswer(this.localSocket)} style={{ alignSelf: 'center'}}>
                Answer
                </Button>
             </View>
             :
             (this.state.busy == true) ?
             <View style={{ flex: 1}}>
             <StatusBar hidden />    
             {remoteVideo}       
             </View>
             :
             null
             }
            </View>     
        </View>              
    </SafeAreaView>
    );
  }
};

const styles = StyleSheet.create({
  buttonsContainer: {
    flexDirection: 'row',
  },
  absolute: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  button: {
    margin: 5,
    paddingVertical: 10,
    backgroundColor: 'lightgrey',
    borderRadius: 5,
  },
  textContent: {
    fontFamily: 'Avenir',
    fontSize: 20,
    textAlign: 'center',
  },
  videosContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: '#010101'
  },
  rtcView: {
    width: windowWidth, //dimensions.width,
    height: windowHeight,//dimensions.height / 2,
    backgroundColor: 'black',
  },
  rtcViewC: {
    width: windowWidth/3.2, //dimensions.width,
    height: windowHeight/3.6,//dimensions.height / 2,
    bottom: 20,
    position: 'relative',
    right: 0,
    zIndex: 2,
    overflow: 'hidden'
  },
  scrollView: {
    flex: 1,
    // flexDirection: 'row',
    backgroundColor: 'teal',
    padding: 15,
  },
  rtcViewRemote: {
    width: windowWidth,
    height: windowHeight,
    backgroundColor: 'black',
  },
  profileImg: {
    width: 50,
    height: 50,
    borderRadius: 25
  }
});

export default CallScreen;