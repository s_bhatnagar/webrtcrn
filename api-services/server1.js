var db = require('./db')
const express = require('express')
var io = require('socket.io')
  ({
    path: '/io/webrtc'
  })

const app = express()
const port = 8080
var offerfrom;
// app.get('/', (req, res) => res.send('Hello World!!!!!'))

//https://expressjs.com/en/guide/writing-middleware.html
app.use(express.static(__dirname + '/build'))
app.get('/', (req, res, next) => {
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.sendFile(__dirname + '/build/index.html')
})

const server = app.listen(port, () => console.log(`Example app listening on port ${port}!`))

io.listen(server)

// default namespace
io.on('connection', socket => {
  console.log('connected')
})

// https://www.tutorialspoint.com/socket.io/socket.io_namespaces.htm
const peers = io.of('/webrtcPeer')

// keep a reference of all socket connections
let connectedPeers = new Map()
var clients = [];
var newClients = [];
var caller = '';
var cameraUser ='';
var cameraStatus ='';

peers.on('connection', socket => {

  socket.on('storeClientInfo', function (data, res) {
    caller = data.username;
    var clientInfo = new Object();
    clientInfo.username = data.username;
    clientInfo.clientId = socket.id;
    clients.push(clientInfo);
    console.log('Clients', clients)
    var sql = `INSERT INTO user (username, clientID) VALUES ('${data.username}','${socket.id}')`
    db.query(sql, function(err, row, fields) {
      if (err) {
          console.log('Error')
      }
      console.log('Success')
  })
  });
  
  socket.on('cameraOffAlert', function(data) {
        // send to the other peer(s) if any
        for (const [socketID, socket] of connectedPeers.entries()) {
          // don't send to self
          if (socketID === data.clientID) {
            socket.emit('videoUpdateUser', {cameraUser: data.username})
            socket.emit('videoUpateCamera', {cameraStatus: data.cameraOff})
            console.log('caller', caller);
            // console.log('calling to', data.clientID);
            // console.log(socketID, data.payload.type, data.clientID)
            // console.log('data', data.payload.sdp)
          }
        }
  })

  socket.emit('users', { users: clients })

  socket.emit('connection-success', { success: socket.id })

  connectedPeers.set(socket.id, socket)

  socket.on('disconnect', function (data) {

    for( var i=0, len=clients.length; i<len; ++i ){
        var c = clients[i];

        if(c.clientId == socket.id){
            username = c.username;
            client = c.clientId;
            clients.splice(i,1);
            console.log('Disconnected', c);
            var sql = `DELETE From user WHERE clientID='${socket.id}'`
            db.query(sql, function(err, row, fields) {
              if (err) {
                  console.log('Error')
              }
              delete clients.username;
              delete clients.client;
              console.log('client', clients)
              socket.emit('users', { users: clients })
              console.log('Disconnected from DB')
          })
            break;
        }
    }
});

  socket.on('offer', (data) => {
    // send to the other peer(s) if any
    for (const [socketID, socket] of connectedPeers.entries()) {
      // don't send to self
      if (socketID === data.clientID) {
        offerfrom = data.socketID;
        socket.emit('offerfrom', { offerfrom, caller });
        console.log('caller', caller);
        // console.log('calling to', data.clientID);
        // console.log(socketID, data.payload.type, data.clientID)
        // console.log('data', data.payload.sdp)
        socket.emit('offerOrAnswer', data.payload)
      }
    }
  })

  socket.on('Answer', (data) => {
    // send to the other peer(s) if any
    for (const [socketID, socket] of connectedPeers.entries()) {
      // don't send to self
      if (socketID === offerfrom) {
        console.log('Answering to', offerfrom);
        // console.log(socketID, data.payload.type, data.clientID)
        // console.log('data', data.payload.sdp)
        socket.emit('offerOrAnswer', data.payload)
      }
    }
  })

  socket.on('candidate', (data) => {
    // send candidate to the other peer(s) if any
    for (const [socketID, socket] of connectedPeers.entries()) {
      // don't send to self
      if (socketID === data.clientID ) {
        // console.log('DATA', data)
        // console.log(socketID, data.payload)
        socket.emit('candidate', data.payload)
      }
    }
  })

  socket.on('endCall', function(data) {
    // send to the other peer(s) if any
    for (const [socketID, socket] of connectedPeers.entries()) {
      // send to both
      if (socketID === data.callee && socketID == data.caller) {
         socket.emit('stopCall', {callee: data.callee, caller: data.caller})
      }
    }
})

})